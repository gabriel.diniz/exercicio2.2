import java.util.Scanner;

public class exercicio2 {

    public static void main(String[] args) { // metodo publico estatico

        exercicio2(); //método
    }

        public static void exercicio2(){ //método

        Scanner in = new Scanner(System.in);

        int valor1, valor2, valor3, media1, valor4, valor5, valor6, media2; //variaveis

        System.out.println("Insira valor da média 1: "); // inserir o valor
        valor1 = in.nextInt();

        System.out.println("Insira valor da média 2: "); // inserir o valor
        valor2 = in.nextInt();

        System.out.println("Insira valor da média 3: "); // inserir o valor
        valor3 = in.nextInt();

        System.out.println("Insira valor da média 4: "); // inserir o valor
        valor4 = in.nextInt();

        System.out.println("Insira valor da média 5: "); // inserir o valor
        valor5 = in.nextInt();

        System.out.println("Insira valor da média 6: "); // inserir o valor
        valor6 = in.nextInt();

        media1 = (valor1 + valor2 + valor3) / 3;
        System.out.println("A média 1 é: "+media1); // imprimir a a média 1

        media2 = (valor4 + valor5 + valor6) / 3;
        System.out.println("A média 2 é: "+media2); // imprimir a média 2

        System.out.println("A soma das médias são: "+(media1+media2));  // imprimir a soma das médias

    }

}
